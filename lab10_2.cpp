﻿// lab10_2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

struct TStudent
{
    string name; 
    string numBook; 
    int course; 
    float rank; 
};

int main()
{
 
    int n_students = 3; 
    TStudent* pS;

    try
    {
        
        pS = new struct TStudent[n_students];
    }
    catch (bad_alloc ba)
    {
        cout << "Память не выделена." << endl;
        cout << ba.what() << endl;
        return -1;
    }

    pS->name = "Johnson J.";
    pS->numBook = "12389239";
    pS->rank = 3.93;
    pS->course = 4;

   
    (pS + 1)->name = "Sullivan";
    (pS + 1)->numBook = "20930032";
    (pS + 1)->rank = 4.98;
    pS[1].course = 3;

    pS[2].name = "Abram F.D.";
    pS[2].numBook = "l2l28983";
    pS[2].rank = 4.32;
    pS[2].course = 5;

    cout << pS->name.c_str() << endl; 
    cout << pS[1].rank << endl;      
    cout << (pS + 2)->numBook.c_str() << endl;     

    delete[] pS;
    return 0;
}